# Brainstorm

01. Ideas/Brainstorm
02. Type of site
03. Plan the pages
04. Create the sitemap
05. Develop navigational structure
06. Plan and write the content (text, images etc.)
    - Choose typography
    - Select colour theme
07. Build the prototypes
    - Divide the layout
08. Test and learn
    - Pay attention to the details
    - Sharpen your work
    - Show your work in progress
09. Design and build the website
    - Design the best case scenario but prepare for the worst
    - Obsess over the design until you hate it
    - Love each of your ideas but don't get too attached to them
    - Code
    - Redo step 08 and 09 'til the end
10. Publish

---
## 01

- presentation/profile (home)
- articles (menu - blog)
- GooRoo (menu)
- Pro Tips (menu)
- Skills (home)
- Follow (menu - GitHub)
- Curriculum (menu - SO Careers / Linkedin)
- Contact (home): icons and form
---

## 02

Professional web site (not to sell, but to introduce a professional)
---

## 03

Home Page:
  - menu (top)
  - profile (GooRoo, Web Dev, Like to Code, In the Community)
  - skills
  - GitHub Activity
  - contact
---

## 04 e 05

- Single Page Website
- Menu:
    - Home;
    - Articles: The Cloudeveloper, GooRoo, Pro Tips;
    - Code samples: GitHub, BitBucket & GitLab;
    - Curriculum: Developer History, LinkedIn, Geek & PDF;
---

## 06

Typo: calibri, sans-serif
Colors: purple: #674172, #6C2A6A; #261300,
        grey: #6f6f6f #1b1b1b, #3C3C3C

Curriculum:

Summary: Node/.NET Core Web Developer.
I like to play with Promises in Node/JS in an event-driven manner and the facilities that <.net feature> gives to <benefit>.
Despite not being an expert in Web Design (far away from that),
I simply love playing around with several JS and CSS frameworks and libraries,
/* including the ones I've created for fun: cheers.css and x-disco.js*/.
Beyond gaming and coding, I also like to play the bass guitar and going outside for street racing, planning to run a marathon some day.


I have built APIs with .NET and Node.js.
You know when to ask more questions and where to look for answers.
You're comfortable with Node.js and front-end development in general.
You're organized and you don't need to tell people about it. It shows.
You love trying out new languages, frameworks, libraries, and leveraging them off, whenever necessary.
You're always looking to advance your skills and work along Senior Developers committed to helping you do that!
You believe that the actual language is less important than the thrill of real users finding value in your creation.
You offer fullstack ability, believe the power of open source solutions, and always have scalability in the back of your mind.
You can jump into projects, review code, suggest improvements, and create trust in clients. You’re never afraid to get your hands dirty in code.
You’re looking for projects that you'll be proud to share with your friends and family, and you're interested in scaling large projects that disrupt markets and build business.

- APIs
- Search ability
- front-end development
- organization
- fullstack ability
- scalability

---

Skills:
- C#/ASP.NET (expert):
I have a deep experience with C# and ASP.NET. I worked with simple and small apps and with huge ones.
Commonly, I use the DDD approach to solve problems no matter the size of the project.
In enterprises I always use Visual Studio as IDE, but in my personal projects, since I use linux (Red Hat based), I like to use VS Code to build programs.
I worked a lot with ASP.NET Webfors and MVC, but honestly, ASP.NET Core is a lot easier to get up and running (using microservices).
Probably I do not want to work with Webforms anymore, the platform is very good and has evolved, but it is not for me. I like to control my code, the HTTP protocol and that is why I also like node.js.

- JavaScript (expert):
JavaScript is my first love. In my first years of career, I never used it much, but always played much. That is reflected in my certifcations involving the language.
Node is almost entirely about JS, so that explains a lot why I use it in side/personal projects. Also, I work with Node creating microservices.

- Go (lang):
I am using Hugo in my Blog (a blog engine made with Go), but before that, I already had some passion about how easy and fast is Go.
I started studying Go just for fun, but now I contribute to open source projects and code fist class microservices using it.

- HTML5 & CSS3 (proficient)
- SQL/NoSQL (Beginner)
- Git/TFS/SVN (proficient)
- Environment (linux/windows) (proficient)
- Also: I work with a variety of things, I do not know how I survived all theses technologies, but here I am.
Angular 2, .NET Core, Express, Ember.js, Mithril, KendoUI, PrimeUI, EXT.js, Pure.css, Bootstrap, TypeScript, Java, Node.js, AJAX, jQuery & Cia, Gulp, Webpack e Grunt and the list goes on, but I think I mentioned the most relevant ones.
---

Open Source and Community:
* put the octocat jedi image here
- Set of MV* JS frameworks components
- Contribute to Mithril
- Contribute to Hugo
- Active participant in GDG Curitiba
- Somewhat participant in DOTNET Curitiba
- Always going in the Capi Codes events (fusion of GDG, DOTNET and WTM here in Curitiba)
- Always going to the - relevant - Meetups
---

Work experience:
- IT Analyst - Sanepar - 2015-current
Skills: node, .net, java etc.
Overview: blablabla
Highlights: created four documents (called there as guides): Architecture of .NET projects, Arch of Node, How to work with Git and Culture of Scrum, also, teached these techies periodically.

- Developer - Ágili - 2014-2015
Skills: node, .net, java etc.
Overview: blablabla
Highlights: created four documents (called there as guides): Architecture of .NET projects, Arch of Node, How to work with Git and Culture of Scrum, also, teached these techies periodically.

- IT Analyst - Sicoob - 2013-2014
Skills: node, .net, java etc.
Overview: blablabla
Highlights: created four documents (called there as guides): Architecture of .NET projects, Arch of Node, How to work with Git and Culture of Scrum, also, teached these techies periodically.

- Programmer - Gelt - 2012-2013
Skills: node, .net, java etc.
Overview: blablabla
Highlights: created four documents (called there as guides): Architecture of .NET projects, Arch of Node, How to work with Git and Culture of Scrum, also, teached these techies periodically.

- Web Design Intern - IAPAR - 2012-2012
Skills: HTML, CSS, Photoshop etc.
Overview: blablabla
Highlights: created four documents (called there as guides): Architecture of .NET projects, Arch of Node, How to work with Git and Culture of Scrum, also, teached these techies periodically.
---

## 07

Take a look at 01, 03, 04 & 05.
---

## 08

Test and learn
---

## 09

Design and code
---

## 10

Publish
---

### Total time: 15h
