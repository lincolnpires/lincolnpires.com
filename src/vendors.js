
// Core
const jquery = require('jquery');
const jQuery = require('jquery');
const $ = require('jquery');
const migrate = require('jquery-migrate');
const bootstrap = require('bootstrap');

// plugins
const easing = require('../vendor/jquery.easing-1.3/jquery.easing.js');
const smoothScroll = require('jquery-smooth-scroll');
const wowjs = require('wowjs');
const parallax = require('../vendor/parallax.js-1.4.2/parallax.js');
const appear = require('../vendor/jquery.appear/jquery.appear.js');
const masonryLayout = require('masonry-layout');
const imagesloaded = require('imagesloaded');
