# Welcome to the source code of lincolnpires.com website

I haven't made big updates in this release, so it�s **mostly** like that previous site you saw before.

* I've created a new site without using ASP.NET.
* Now I am using a template (and not my own CSS and JS) that I edited.
* Tried to let the site more *professional*.

This is dead