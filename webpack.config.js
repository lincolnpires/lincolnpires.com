const webpack = require('webpack');
const Path = require('path');
var Html = require('html-webpack-plugin');
var ExtractText = require('extract-text-webpack-plugin');
var Copy = require('copy-webpack-plugin');

const config = {

  entry: {
    styles: './src/styles.js',
    vendors: './src/vendors.js',
    app: './src/app.js',
  },

  output: {
    path: Path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].bundle.js'
  },

  module: {
    rules: [{
      test: /\.css$/,
      use: ExtractText.extract({
        fallback: 'style-loader',
        use: ['css-loader'],
        publicPath: './dist'
      })
    }]
  },

  plugins: [
    new Html({
      minify: {
        collapseWhitespace: true
      },
      template: './src/index.html'
    }),

    new ExtractText({
      filename: '[name].[contenthash].bundle.css',
      disable: false,
      allChunks: true
    }),

    new Copy([{
      from: 'src/favicon.ico'
    }, {
      context: 'src/images',
      from: '**/*',
      to: 'images'
    }, {
      from: 'src/fonts',
      to: 'fonts'
    }, {
      context: 'src/error',
      from: '**/*.html',
      to: 'error'
    }])
  ],

  devServer: {
    contentBase: Path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    stats: 'errors-only',
    open: false
  }

};

module.exports = config;
